/*
GPS RX -> D7 TX -> D8
SD Card CS -> D9

*/

#include <Adafruit_GPS.h>
#include <SoftwareSerial.h>
#include <SD.h>

SoftwareSerial gpsSerial(8, 7);
Adafruit_GPS GPS(&gpsSerial);

File logFile;
char buffer[11];

void setup() {
  GPS.begin(9600);
  GPS.sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  GPS.sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
  GPS.sendCommand(PGCMD_ANTENNA);
  
  pinMode(SS, OUTPUT);
  if (!SD.begin(9)) {
    Serial.println(F("Problem with SD card: re-insert card and rest device!"));
    //flash red SD ERROR LED
    return;
  }
  
  Serial.begin(115200);
}


uint32_t timer = millis();
uint32_t timer_1s = millis();

void loop() {
  
  // if a sentence is received, we can check the checksum, parse it...
  char c = GPS.read();
  if (GPS.newNMEAreceived()) {  
    if (!GPS.parse(GPS.lastNMEA())){   // this also sets the newNMEAreceived() flag to false
      return;  // we can fail to parse a sentence in which case we should just wait for another
    }
  }
  
  // if millis() or timer wraps around, we'll just reset it
  if (timer > millis())
    timer = millis();
  
  if (millis() - timer > 200) { 
    timer = millis(); // reset the timer
    
    WriteLog(FileNameFromGPS(GPS));
  }
  
  if (millis() - timer_1s > 1000) { 
    timer_1s = millis(); // reset the timer
    
    char __dataFileName[19];
    FileNameFromGPS(GPS).toCharArray(__dataFileName, 19);
    Serial.println(__dataFileName);
  }
}





//return GPS cord -- this comment sucks... 
String getCord(float cord,String head)
{  
  dtostrf(decimalDegrees(cord),2,6,buffer);
  
  if((String)head == "W" || (String)head == "S")
    return ((String)"-"+buffer);
  else
    return ((String)"+"+buffer);
} 


// convert NMEA coordinate to decimal degrees
float decimalDegrees(float nmeaCoord) {
  uint16_t wholeDegrees = 0.01*nmeaCoord;
  return wholeDegrees + (nmeaCoord - 100.0*wholeDegrees)/60.0;
}


String FileNameFromGPS(Adafruit_GPS GPS){
  String year = "20" + AddZeroIfLessThanTen(GPS.year);
  String month = AddZeroIfLessThanTen(GPS.month);
  String day = AddZeroIfLessThanTen(GPS.day);
  return (String)(year + month + day + ".csv");
}

String AddZeroIfLessThanTen(int num){
  if(num < 10)
     return "0" + (String)num;
  else
    return (String)num;
}

bool WriteLog(String fileName){
  char __dataFileName[19];
  fileName.toCharArray(__dataFileName, 19);
  
  logFile = SD.open(__dataFileName, FILE_WRITE);
  
  //lat
  logFile.print(getCord(GPS.latitude,(String)GPS.lat));
  logFile.print(F(","));
  //lon
  logFile.print(getCord(GPS.longitude,(String)GPS.lon));
  logFile.print(F(","));
  //mph
  logFile.print(dtostrf(GPS.speed*1.15078,3,0,buffer));
  logFile.print(F(","));  
  //altitude
  logFile.print(dtostrf(GPS.altitude,5,0,buffer));
  logFile.print(F(","));
  //angle
  logFile.print(dtostrf(GPS.angle,5,0,buffer));
  logFile.print(F(","));
  //year
  logFile.print(dtostrf(GPS.year,2,0,buffer));
  logFile.print(F(","));
  //month
  logFile.print(dtostrf(GPS.month,2,0,buffer));
  logFile.print(F(","));
  //day
  logFile.print(dtostrf(GPS.day,2,0,buffer));
  logFile.print(F(","));
  //hour
  logFile.print(dtostrf(GPS.hour,2,0,buffer));
  logFile.print(F(","));
  //minute
  logFile.print(dtostrf(GPS.minute,2,0,buffer));
  logFile.print(F(","));
  //second
  logFile.print(dtostrf(GPS.seconds,2,0,buffer));
  logFile.print(F("."));
  //mili-second
  logFile.print(dtostrf(GPS.milliseconds,2,0,buffer));
  logFile.print(F(","));
  //quality
  logFile.print(dtostrf(GPS.satellites,3,0,buffer));
 
  logFile.println(F(""));
  logFile.flush();
  
  logFile.close();
    
  return true;
}

